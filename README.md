# README #

A puppet module to install and configure an instance of Solr 5.0 and above.

## Usage ##

### Single instance, single core ###
```
#!puppet

node 'mynode' {
  # The class just establishes the user under who runs all instances
  class{'solr5':
    solr_user => 'puppetadmin',
    solr_group => 'puppetadmin'
  }
 
  # For each solr instance, declare a solr5:instance type
  # see manifests/instance.pp for all possible params, e.g. port, java memory allocation etc.
  # cores are optional but if defined, they must be given as the full path in the form $base_var_dir/$instance_name/data/[core name]
  # where $base_var_dir is an existing folder on the server (can be overwritten as $base_var_dir)
  # and $instance_name is the name/title assigned to this instance
  # and [core name] corresponds to a folder in the files/ folder of this module, e.g. files/parish_registers, files/nlihydra
  solr5::instance{'my_instance':
    solr_cores => ["/var/my_instance/data/parish_registers",],
    require      => Class['solr5']
  }

}
```

### Single instance, multi-core ###
```
#!puppet

node 'mynode' {
  class{'solr5':
    solr_user => 'puppetadmin',
    solr_group => 'puppetadmin'
  }
 
  # An array of cores
  solr5::instance{'my_instance':
    solr_cores => ["/var/my_instance/data/parish_registers", "/var/my_instance/data/core2",],
    require      => Class['solr5']
  }

}
```

### Multi-instance, multi-core ###
```
#!puppet

node 'mynode' {
  class{'solr5':
    solr_user => 'puppetadmin',
    solr_group => 'puppetadmin'
  }
 
  solr5::instance{'my_instance':
    solr_cores => ["/var/my_instance/data/parish_registers", "/var/my_instance/data/core2",],
    require      => Class['solr5']
  }

  solr5::instance{'my_second_instance':
    # we need to specify a port other than the default to avoid conflicts
    solr_port => '1111'
    # we might want to run different versions in parallel (optional)
    solr_version => '5.1.0'
    solr_cores => ["/var/my_second_instance/data/parish_registers", "/var/my_second_instance/data/other_core",],
    require      => Class['solr5']
  }

}
```