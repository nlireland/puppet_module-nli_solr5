<?xml version="1.0" encoding="UTF-8"?>
<schema name="Parish Registers" version="0.2">
  <!-- NOTE: various comments and unused configuration possibilities have been purged
     from this file.  Please refer to http://wiki.apache.org/solr/SchemaXml,
     as well as the default schema file included with Solr -->

  <uniqueKey>id</uniqueKey>

  <fields>
    <!-- If you remove this field, you must _also_ disable the update log in solrconfig.xml
         or Solr won't start. _version_ and update log are required for SolrCloud
    -->
    <field name="_version_" type="long" indexed="true" stored="true"/>

    <field name="id" type="string" stored="true" indexed="true" multiValued="false" required="true"/>
    <field name="doc_type" type="string" indexed="true" stored="true" required="true" />
    <field name="timestamp" type="date" indexed="true" stored="true" default="NOW" multiValued="false"/>
    <!-- We'll use Hydra conventions for dynamic field suffixes in parish registers -->
    <!-- NOTE:  not all possible Solr field types are represented in the dynamic fields -->

    <!-- text (_t...) -->
    <dynamicField name="*_ti" type="text" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_tim" type="text" stored="false" indexed="true" multiValued="true"/>
    <dynamicField name="*_ts" type="text" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_tsm" type="text" stored="true" indexed="false" multiValued="true"/>
    <dynamicField name="*_tsi" type="text" stored="true" indexed="true" multiValued="false"/>
    <dynamicField name="*_tsim" type="text" stored="true" indexed="true" multiValued="true"/>
    <dynamicField name="*_tiv" type="text" stored="false" indexed="true" multiValued="false" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_timv" type="text" stored="false" indexed="true" multiValued="true" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_tsiv" type="text" stored="true" indexed="true" multiValued="false" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_tsimv" type="text" stored="true" indexed="true" multiValued="true" termVectors="true" termPositions="true" termOffsets="true"/>

    <!-- English text (_te...) -->
    <dynamicField name="*_tei" type="text_en" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_teim" type="text_en" stored="false" indexed="true" multiValued="true"/>
    <dynamicField name="*_tes" type="text_en" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_tesm" type="text_en" stored="true" indexed="false" multiValued="true"/>
    <dynamicField name="*_tesi" type="text_en" stored="true" indexed="true" multiValued="false"/>
    <dynamicField name="*_tesim" type="text_en" stored="true" indexed="true" multiValued="true"/>
    <dynamicField name="*_teiv" type="text_en" stored="false" indexed="true" multiValued="false" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_teimv" type="text_en" stored="false" indexed="true" multiValued="true" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_tesiv" type="text_en" stored="true" indexed="true" multiValued="false" termVectors="true" termPositions="true" termOffsets="true"/>
    <dynamicField name="*_tesimv" type="text_en" stored="true" indexed="true" multiValued="true" termVectors="true" termPositions="true" termOffsets="true"/>

    <!-- string (_s...) -->
    <dynamicField name="*_si" type="string" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_sim" type="string" stored="false" indexed="true" multiValued="true"/>
    <dynamicField name="*_ss" type="string" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_ssm" type="string" stored="true" indexed="false" multiValued="true"/>
    <dynamicField name="*_ssi" type="string" stored="true" indexed="true" multiValued="false"/>
    <dynamicField name="*_ssim" type="string" stored="true" indexed="true" multiValued="true"/>
    <dynamicField name="*_ssort" type="alphaSort" stored="false" indexed="true" multiValued="false"/>

    <!-- integer (_i...) -->
    <dynamicField name="*_ii" type="int" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_iim" type="int" stored="false" indexed="true" multiValued="true"/>
    <dynamicField name="*_is" type="int" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_ism" type="int" stored="true" indexed="false" multiValued="true"/>
    <dynamicField name="*_isi" type="int" stored="true" indexed="true" multiValued="false"/>
    <dynamicField name="*_isim" type="int" stored="true" indexed="true" multiValued="true"/>

    <!-- trie date (_dtt...) (for faster range queries) -->
    <dynamicField name="*_dtti" type="tdate" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_dttim" type="tdate" stored="false" indexed="true" multiValued="true"/>
    <dynamicField name="*_dtts" type="tdate" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_dttsm" type="tdate" stored="true" indexed="false" multiValued="true"/>
    <dynamicField name="*_dttsi" type="tdate" stored="true" indexed="true" multiValued="false"/>
    <dynamicField name="*_dttsim" type="tdate" stored="true" indexed="true" multiValued="true"/>

    <!-- boolean (_b...) -->
    <dynamicField name="*_bi" type="boolean" stored="false" indexed="true" multiValued="false"/>
    <dynamicField name="*_bs" type="boolean" stored="true" indexed="false" multiValued="false"/>
    <dynamicField name="*_bsi" type="boolean" stored="true" indexed="true" multiValued="false"/>

   <!--
     Dynamic Field for spelling.
     Taken from Blacklight.
     -->
   <dynamicField name="*spell" type="textSpell" indexed="true" stored="false" multiValued="true" />

   <!--
     Dynamic Field for spelling.
     Taken from Blacklight.
     -->
   <dynamicField name="*_suggest" type="text_suggest" indexed="true" stored="true" multiValued="true" />

    <!--
      Removing the following Hydra dynamic types that we don't need for parish registers
      Restore as needed
      -->

    <!-- trie integer (_it...) (for faster range queries) -->
    <!-- date (_dt...) -->
    <!-- The format for this date field is of the form 1995-12-31T23:59:59Z
         Optional fractional seconds are allowed: 1995-12-31T23:59:59.999Z -->
    <!-- trie date (_dtt...) (for faster range queries) -->
    <!-- long (_l...) -->
    <!-- trie long (_lt...) (for faster range queries) -->
    <!-- double (_db...) -->
    <!-- trie double (_dbt...) (for faster range queries) -->
    <!-- float (_f...) -->
    <!-- trie float (_ft...) (for faster range queries) -->
    <!-- Type used to index the lat and lon components for the "location" FieldType -->
    <!-- location (_ll...) -->
    <!-- you must define copyField source and dest fields explicity or schemaBrowser doesn't work -->

  </fields>

  <!-- copy fields;  note that you must define copyField source and dest fields explicity or schemaBrowser doesn't work -->
  <!--
    <copyField source="some_field" dest="all_text_timv" />
  -->

  <!-- Modified from Blacklight
       We'll need to add over copyFields as we refine the
       parish register spellcheck
    -->
  <!-- spellcheck fields -->
  <!-- We only want spelling suggestions from these specific fields -->
  <copyField source="parish_name_tesi" dest="spell"/>
  <copyField source="counties_tesim" dest="spell"/>
  <copyField source="diocese_name_tesi" dest="spell"/>
  <copyField source="variants_tesim" dest="spell"/>

  <!-- copyfield for suggestions -->
  <copyField source="parish_suggest" dest="combined_suggest"/>
  <copyField source="variants_suggest" dest="combined_suggest"/>
  <types>
    <!--
      Removing Hydra field types not used in parish registers
      Restore as needed
      -->
    <fieldType name="string" class="solr.StrField" sortMissingLast="true" />
    <fieldType name="boolean" class="solr.BoolField" sortMissingLast="true"/>

    <!-- Default numeric field types.  -->
    <fieldType name="int" class="solr.TrieIntField" precisionStep="0" positionIncrementGap="0"/>
    <fieldType name="long" class="solr.TrieLongField" precisionStep="0" positionIncrementGap="0"/>

    <!-- The format for this date field is of the form 1995-12-31T23:59:59Z
         Optional fractional seconds are allowed: 1995-12-31T23:59:59.999Z
      -->
    <fieldType name="date" class="solr.TrieDateField" precisionStep="0" positionIncrementGap="0"/>

    <!-- A Trie based date field for faster date range queries and date faceting. -->
    <fieldType name="tdate" class="solr.TrieDateField" precisionStep="6" positionIncrementGap="0"/>


    <!-- General text field -->
    <fieldType name="text" class="solr.TextField" omitNorms="false">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StandardFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.TrimFilterFactory"/>
      </analyzer>
    </fieldType>

    <!-- single token analyzed text, for sorting.  Punctuation is significant. -->
    <fieldtype name="alphaSort" class="solr.TextField" sortMissingLast="true" omitNorms="true">
      <analyzer>
        <tokenizer class="solr.KeywordTokenizerFactory" />
        <filter class="solr.StandardFilterFactory"/>
        <filter class="solr.TrimFilterFactory" />
      </analyzer>
    </fieldtype>

    <!-- A text field with defaults appropriate for English -->
    <fieldType name="text_en" class="solr.TextField" positionIncrementGap="100">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StandardFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="stopwords.txt"/>
        <filter class="solr.SynonymFilterFactory" synonyms="synonyms.txt" ignoreCase="true"/>
        <filter class="solr.EnglishPossessiveFilterFactory"/>
        <!-- EnglishMinimalStemFilterFactory is less aggressive than PorterStemFilterFactory: -->
        <filter class="solr.EnglishMinimalStemFilterFactory"/>
        <!--
        <filter class="solr.PorterStemFilterFactory"/>
        -->
        <filter class="solr.TrimFilterFactory"/>
      </analyzer>
    </fieldType>

    <!-- A text field for use with autosuggest -->
    <fieldType name="text_suggest" class="solr.TextField" positionIncrementGap="100">
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StandardFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.EnglishPossessiveFilterFactory"/>
        <filter class="solr.TrimFilterFactory"/>
      </analyzer>
    </fieldType>

    <!-- Spell fieldType taken from Blacklight -->
    <fieldType name="textSpell" class="solr.TextField" positionIncrementGap="100" >
      <analyzer>
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.StandardFilterFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
        <filter class="solr.StopFilterFactory" ignoreCase="true" words="stopwords.txt"/>
        <filter class="solr.RemoveDuplicatesTokenFilterFactory"/>
      </analyzer>
    </fieldType>
  </types>
</schema>
