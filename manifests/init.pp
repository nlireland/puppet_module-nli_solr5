class solr5 (
  $solr_user     = $solr5::params::solr_user,
  $solr_group    = $solr5::params::solr_group,
) inherits solr5::params {
  include 'java'
  include 'archive'
}
