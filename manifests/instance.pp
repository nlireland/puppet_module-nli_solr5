# setting a newrelic license key requires:
# solr_opts => '-javaagent:[solr_var_dir]/newrelic/newrelic.jar',
define solr5::instance (
  $instance_name        = $title,
  $solr_version         = '5.0.0',
  $server_host          = $::fqdn,
  $solr_port            = '8983',
  $solr_cores           = {},
  $base_var_dir         = '/var',
  $base_lib_dir         = '/opt',
  $solr_java_mem        = '-Xms512m -Xmx512m',
  $solr_download_url    = 'UNDEF',
  $enable_newrelic      = false,
  $newrelic_license_key = 'UNDEF',
  $solr_opts            = ''
) {

  if ($enable_newrelic == true) and ($newrelic_license_key == 'UNDEF') {
    fail('The newrelic_license_key parameter must be defined if $enable_newrelic  is set to true.')
  }

  include 'solr5'
  $solr_user = $solr5::solr_user
  $solr_group = $solr5::solr_group

  $solr_var_dir     = "$base_var_dir/$instance_name"
  $solr_lib_dir = "$base_lib_dir/$instance_name"

  # interpolating this in the params doesn't seem to work
  # see https://tickets.puppetlabs.com/browse/PUP-1080
  if $solr_download_url == 'UNDEF' {
    $download_url = "https://archive.apache.org/dist/lucene/solr/$solr_version/solr-$solr_version.tgz"
  } else {
    $download_url = $download_url
  }

  file { ["$solr_lib_dir", "$solr_var_dir", "$solr_var_dir/data", "$solr_var_dir/logs"]:
    ensure  => directory,
    owner   => $solr_user,
    group   => $solr_group,
  }

  archive { "/tmp/$instance_name-solr-$solr_version.tar.gz":
    ensure        => present,
    extract       => true,
    extract_path  => $solr_lib_dir,
    source        => $download_url,
    creates       => "$solr_lib_dir/solr-$solr_version",
    cleanup       => true,
    require       => File["$solr_lib_dir"]
  }->
  exec{"$instance_name-chown":
    command => "/bin/chown -R $solr_user:$solr_group $solr_lib_dir/solr-$solr_version/server"
  }

  file {"$solr_var_dir/data/solr.xml":
    ensure  => file,
    source => "$solr_lib_dir/solr-$solr_version/server/solr/solr.xml",
    require => Archive["/tmp/$instance_name-solr-$solr_version.tar.gz"],
  }

  file {"$solr_var_dir/solr.in.sh":
    ensure  => file,
    source => "$solr_lib_dir/solr-$solr_version/bin/solr.in.sh",
    require => Archive["/tmp/$instance_name-solr-$solr_version.tar.gz"],
  }

  file {"$solr_var_dir/log4j.properties":
    ensure  => file,
    source => "$solr_lib_dir/solr-$solr_version/server/resources/log4j.properties",
    require => File["$solr_var_dir/solr.in.sh"],
  }

  augeas {"$instance_name-log4j-edits":
    lens => 'Properties.lns',
    incl => "$solr_var_dir/log4j.properties",
    changes => ['set solr.log ${solr.solr.home}/../logs',],
    require => File["$solr_var_dir/log4j.properties"],
  }

  augeas {"$instance_name-solr-env-vars-edits":
    lens => 'Properties.lns',
    incl => "$solr_var_dir/solr.in.sh",
    changes   => [
      "set SOLR_PID_DIR $solr_var_dir",
      "set SOLR_HOME $solr_var_dir/data",
      "set LOG4J_PROPS $solr_var_dir/log4j.properties",
      "set SOLR_LOGS_DIR $solr_var_dir/logs",
      "set SOLR_PORT $solr_port",
      "set SOLR_JAVA_MEM  '\"$solr_java_mem\"'",
      "set SOLR_OPTS  '\"\$SOLR_OPTS $solr_opts\"'",
    ],
    require => File["$solr_var_dir/solr.in.sh"],
  }

  #### Newrelic java monitoring for solr
  if ($enable_newrelic == true) {
    archive { "/tmp/newrelic-java.zip":
      ensure        => present,
      extract       => true,
      extract_path  => "$solr_var_dir",
      source        => "https://download.newrelic.com/newrelic/java-agent/newrelic-agent/current/newrelic-java.zip",
      cleanup       => true,
      creates       => "$solr_var_dir/newrelic",
      require       => [Package['unzip'], File["$solr_var_dir"]]
    }->
    file{"$solr_var_dir/newrelic/newrelic.yml":
      ensure    => present,
      owner     => $deploy_user,
      group     => $deploy_group,
      path      => "$solr_var_dir/newrelic/newrelic.yml",
      content   => template('solr5/newrelic_yaml.erb'),
      replace   => true,
      notify  => Service[$instance_name],
    }
  } else {
    file{"$solr_var_dir/newrelic":
      ensure  => absent,
      recurse => true,
      purge   => true,
      force   => true,
      require => File["$solr_var_dir"],
      notify  => Service[$instance_name],
    }
  }

  file {"/etc/init.d/$instance_name":
    ensure  => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
    content => template('solr5/init-script.erb'),
    require => File["$solr_var_dir"],
  }

  $solr_core_defaults = {
    base_lib_dir => $base_lib_dir,
    solr_version => $solr_version,
    require => File["$solr_var_dir"],
    notify  => Service[$instance_name],
  }

  create_resources(solr5::core , $solr_cores, $solr_core_defaults)

  service { $instance_name :
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    subscribe  => [Augeas["$instance_name-solr-env-vars-edits"],],
    require => File["/etc/init.d/$instance_name"],
  }

}
