define solr5::core (
   $core_path  = $title,
   $solr_core_type = 'standalone',
   $solr_master_ip = 'localhost',
   $solr_master_port = '8080',
   $solr_master_core_name = 'default',
   $solr_master_poll_interval = '00:00:20',
   $solr_version         = '5.0.0',
   $base_lib_dir         = '/opt',
  ) {
  include 'solr5'
  $solr_user = $solr5::solr_user
  $solr_group = $solr5::solr_group

  $core_path_parts = split($core_path, "/")
  $core_name = $core_path_parts[-1]

  file { $core_path:
    ensure  => directory,
    owner   => $solr_user,
    group   => $solr_group,
  }

  file { "$core_path/core.properties":
    ensure  => present,
    content => "name=$core_name",
    owner   => $solr_user,
    group   => $solr_group,
    require => File["$core_path", "$core_path/conf"],
  }

  file { "$core_path/conf":
    ensure  => directory,
    source  => "puppet:///modules/solr5/$core_name/conf",
    owner   => $solr_user,
    group   => $solr_group,
    recurse => true,
    require => File["$core_path"],
  }

  augeas { "solr-lib-dirs-$core_path":
    lens => 'Xml.lns',
    incl => "$core_path/conf/solrconfig.xml",
    changes   => [
      "set config/lib[#attribute/dir='../lib/contrib/analysis-extras/lucene-libs']/#attribute/dir $base_lib_dir/solr/solr-$solr_version/contrib/analysis-extras/lucene-libs",
      "set config/lib[#attribute/dir='../lib/contrib/analysis-extras/lib']/#attribute/dir $base_lib_dir/solr/solr-$solr_version/contrib/analysis-extras/lib",
    ],
    require => File["$core_path/conf"],
  }

  if $solr_core_type == 'master' {
    augeas { "solr-master-$core_path":
      lens => 'Xml.lns',
      incl => "$core_path/conf/solrconfig.xml",
      changes   => [
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/#attribute/name master",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='replicateAfter']/#attribute/name replicateAfter",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='replicateAfter']/#text commit",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='backupAfter']/#attribute/name backupAfter",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='backupAfter']/#text commit",

        #confFiles to copy, probably none as everthing should be managed by puppet
        #"set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str/#attribute/name confFiles",
        #"set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='confFiles']/#text schema.xml,stopwords.txt,elevate.xml",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='commitReserveDuration']/#attribute/name commitReserveDuration",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='master']/str[#attribute/name='commitReserveDuration']/#text '00:00:10'",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/int[#attribute/name='maxNumberOfBackups']/#attribute/name maxNumberOfBackups",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/int[#attribute/name='maxNumberOfBackups']/#text 2",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='invariants']/#attribute/name invariants",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='invariants']/str[#attribute/name='maxWriteMBPerSec']/#attribute/name maxWriteMBPerSec",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='invariants']/str[#attribute/name='maxWriteMBPerSec']/#text 16",

      ],
      require => File["$core_path/conf"],
    }
  }
  if $solr_core_type == 'slave' {
    augeas { "solr-slave-$core_path":
      lens => 'Xml.lns',
      incl => "$core_path/conf/solrconfig.xml",
      changes   => [
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='slave']/#attribute/name slave",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='slave']/str[#attribute/name='masterUrl']/#attribute/name masterUrl",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='slave']/str[#attribute/name='masterUrl']/#text 'http://$solr_master_ip:$solr_master_port/solr/$solr_master_core_name/replication'",

        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='slave']/str[#attribute/name='pollInterval']/#attribute/name pollInterval",
        "set config/requestHandler[#attribute/class='solr.ReplicationHandler']/lst[#attribute/name='slave']/str[#attribute/name='pollInterval']/#text $solr_master_poll_interval",
      ],
      require => File["$core_path/conf"],
    }
  }
}
